import { Injectable } from '@angular/core';
import { Monitor } from './monitor';
import { AngularFireDatabase } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class MonitorService {
  constructor(private db: AngularFireDatabase) { }

  insert(aula: Monitor) {
    this.db.list('monitor').push(aula)
      .then((result: any) => {
        alert("Inserido com sucesso!")
      })
  }

  update(aula: Monitor, key: string) {
    this.db.list('monitor').update(key, aula)
      .then((result: any) => {
        alert("Alterado com sucesso!")
      }).catch((error: any) => {
        console.log(error);
      })
  }

  getAll() {
    return this.db.list('monitor')
    .snapshotChanges()
    
  }

  delete(key: string) {
    this.db.object(`monitor/${key}`).remove();
  }
}
