import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CadastroMonitorComponent } from './cadastro-monitor/cadastro-monitor.component';
import { CadastroAulaComponent } from './cadastro-aula/cadastro-aula.component';


const routes: Routes = [
  {path: 'monitor', component: CadastroMonitorComponent},
  {path: 'aula', component: CadastroAulaComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
