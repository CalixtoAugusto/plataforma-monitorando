export class Aula {
    constructor(nome: string, curso: string, complemento: string, key?: string) {
        this.nome = nome;
        this.key = key;
        this.complemento = complemento;
        this.curso = curso;
    }
    nome: string;
    complemento: string;
    key: string;
    curso: string;
}
