import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Monitor } from './monitor';
import { Aula } from '../cadastro-aula/aula';
import { MonitorService } from './monitor.service';

@Component({
  selector: 'app-cadastro-monitor',
  templateUrl: './cadastro-monitor.component.html',
  styleUrls: ['./cadastro-monitor.component.scss']
})
export class CadastroMonitorComponent implements OnInit {

  monitorGroup: FormGroup;
  monitor: Monitor;
  todosMonitores: Monitor[] = [];

  turmas = [
    {value: "COM"},
    {value: "SIN"},
    {value: "MAT"},
    {value: "ENG"},
    {value: "BIO"},
  ]

  
  constructor(private fb: FormBuilder, private cadastroService: MonitorService) { }

  ngOnInit(): void {
    this.monitorGroup = this.fb.group({
      nome: new FormControl('', Validators.required),
      curso: new FormControl('', Validators.required),
      periodo: new FormControl('', Validators.required),
      cpf: new FormControl('', Validators.required),
      materia: new FormControl('', Validators.required),
      complemento: new FormControl('', Validators.required),
      
    })

    this.cadastroService.getAll().subscribe(vals => {
      this.todosMonitores = []
      vals.forEach((val: any) => {
        let x: Monitor = { key: val.key, nome: val.payload.val().nome, curso: val.payload.val().curso, periodo: val.payload.val().periodo, cpf: val.payload.val().cpf, materia: val.payload.val().materia, complemento: val.payload.val().complemento };
        this.todosMonitores.push(x)
      })
    }
    )
  }

  addMonitor() {
    this.monitor = this.monitorGroup.value;
    console.log(this.monitor);
    this.cadastroService.insert(this.monitor);
  }

  editMonitor(key: string) {
    this.monitor = this.monitorGroup.value;
    this.cadastroService.update(this.monitor, key)
  }

  removeMonitor(key:string) {
    this.cadastroService.delete(key);
  }

}
