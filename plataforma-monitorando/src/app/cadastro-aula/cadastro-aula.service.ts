import { Injectable } from '@angular/core';
import { Aula } from './aula';
import { AngularFireDatabase, SnapshotAction } from '@angular/fire/database';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CadastroAulaService {

  constructor(private db: AngularFireDatabase) { }

  insert(aula: Aula) {
    this.db.list('aula').push(aula)
      .then((result: any) => {
        alert("Inserido com sucesso!")
      })
  }

  update(aula: Aula, key: string) {
    this.db.list('aula').update(key, aula)
      .then((result: any) => {
        alert("Usuario alterado com sucesso!")
      }).catch((error: any) => {
        console.log(error);
      })
  }

  getAll() {
    return this.db.list('aula')
    .snapshotChanges()
    
  }

  delete(key: string) {
    this.db.object(`aula/${key}`).remove();
  }
}
