import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { Aula } from './aula';
import { CadastroAulaService } from './cadastro-aula.service'
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-cadastro-aula',
  templateUrl: './cadastro-aula.component.html',
  styleUrls: ['./cadastro-aula.component.scss']
})
export class CadastroAulaComponent implements OnInit {
  aulaGroup: FormGroup;
  aula: Aula;
  todasAulas: Aula[] = [];


  turmas = [
    {value: "COM"},
    {value: "SIN"},
    {value: "MAT"},
    {value: "ENG"},
    {value: "BIO"},
  ]

  constructor(private fb: FormBuilder, private cadastroService: CadastroAulaService) { }

  ngOnInit(): void {
    this.aulaGroup = this.fb.group({
      nome: new FormControl(''),
      complemento: new FormControl(''),
      curso: new FormControl(''),
    })

    this.cadastroService.getAll().subscribe(vals => {
      this.todasAulas = []
      vals.forEach((val: any) => {
        let x: Aula = { key: val.key, nome: val.payload.val().nome, curso: val.payload.val().curso, complemento: val.payload.val().complemento };
        this.todasAulas.push(x)
      })
    }
    )

  }

  addAula() {
    const { nome, turma, complemento } = this.aulaGroup.value;
    this.aula = this.aulaGroup.value;
    console.log(this.aula)
    this.cadastroService.insert(this.aula)
  }

  editAula(key: string) {
    this.aula = this.aulaGroup.value;
    this.cadastroService.update(this.aula, key)
  }

  removeAula(key:string) {
    this.cadastroService.delete(key);
  }

}
