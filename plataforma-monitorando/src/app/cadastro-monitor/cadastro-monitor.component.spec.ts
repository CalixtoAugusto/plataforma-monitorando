import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroMonitorComponent } from './cadastro-monitor.component';

describe('CadastroMonitorComponent', () => {
  let component: CadastroMonitorComponent;
  let fixture: ComponentFixture<CadastroMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
