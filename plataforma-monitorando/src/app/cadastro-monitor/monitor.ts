export class Monitor {
    key: string;
    nome: string;
    curso: string;
    periodo: string;
    cpf: string;
    materia: string;
    complemento: string;
}
