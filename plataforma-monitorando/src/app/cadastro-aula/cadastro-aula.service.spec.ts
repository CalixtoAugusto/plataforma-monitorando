import { TestBed } from '@angular/core/testing';

import { CadastroAulaService } from './cadastro-aula.service';

describe('CadastroAulaService', () => {
  let service: CadastroAulaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CadastroAulaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
